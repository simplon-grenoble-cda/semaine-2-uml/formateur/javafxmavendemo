This repository illustrate how you can setup a JavaFX project with Maven.

In the master branch, you only have a JavaFX project as initialized by IntelliJ. In order to run the
application you have to manually install and configure the dependencies to JavaFX.

In the `maven` branch, you have the same project, modified in order to work with maven. With maven configuration,
you only have to run `mvn install` (in command line or through your IDE interface) in order to install the JavaFX
dependencies. And you can run `mvn javafx:run` to launch the application

